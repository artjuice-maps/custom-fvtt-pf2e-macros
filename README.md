# Custom FVTT PF2E Macros

A collection of macros I've made for various PF2E campaigns

## Installation
The following can be used to install the module.
```
https://gitlab.com/artjuice-maps/custom-fvtt-pf2e-macros/-/raw/main/module.json
```
